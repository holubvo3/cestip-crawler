"use strict";


module.exports = class PageScraper {

    /**
     * @param {PageCrawler} crawler
     */
    constructor(crawler) {
        /**
         * @type {PageCrawler}
         */
        this.crawler = crawler;
        this.handlers = [];
        this.onDataCallback = () => {};
    }

    /**
     * @param {{$: Cheerio}} res
     * @param {URL} url
     * @return {boolean|undefined|Promise}
     */
    handlePage(res, url) {

        for (let handler of this.handlers) {
            const match = url.pathname.match(handler.path);
            if (match && match.length) {
                return handler.fn(res, url, match);
            }
        }

        return false;
    }

    /**
     * @return {string}
     */
    static getBaseUrl() {
        return '';
    }
}