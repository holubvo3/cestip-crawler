"use strict";

const AccommodationType = {
    APARTMENT: 'APARTMENT',
    HOTEL: 'HOTEL',
    BUNGALOW: 'BUNGALOW'
};
const Boarding = {
    FULL_BOARD: 'FULL_BOARD',
    ALL_INCLUSIVE: 'ALL_INCLUSIVE',
    HALF_BOARD: 'HALF_BOARD',
    NONE: 'NONE'
};

const BOARDING_MAP = {
    'all inclusive': Boarding.ALL_INCLUSIVE,
    'plná penze': Boarding.FULL_BOARD,
    'polopenze': Boarding.HALF_BOARD
};

const ACCOMMODATION_MAP = {
    'hotelový pokoj': AccommodationType.HOTEL,
    'apartmán': AccommodationType.APARTMENT,
    'bungalov': AccommodationType.BUNGALOW
};

const API_SEARCH_TERMS_URL = 'https://www.zajezdy.cz/api/detail/loadDetailTerms';
const API_LOAD_TERM_DETAIL_URL = 'https://www.zajezdy.cz/api/detail/loadDetail';
const PEOPLE_VARIANTS = [
    {adults: 2, children: 0},
    {adults: 2, children: 1},
    {adults: 2, children: 2},
    {adults: 2, children: 4},
];

module.exports = class TripPage {

    /**
     * @param {ScraperZajezdy} scraper
     * @param {number} id
     */
    constructor(scraper, id) {
        /**
         * @type {ScraperZajezdy}
         */
        this._scraper = scraper;

        /**
         * @type {number}
         */
        this.id = id;
        this.url = null;
        this.name = null;

        this.destination = null;
        this.travelAgency = null;

        this.accommodation = {
            name: null,
            classification: 0,
            url: null,
            rating: 0,
            lat: 0,
            lon: 0
        };

        this.terms = [];

        this._loadTermUrl = new URL(API_LOAD_TERM_DETAIL_URL);
        this._loadTermUrl.searchParams.set('tourId', this.id + '');
        this._loadTermUrl.searchParams.set('allTerms', 'false');
        this._loadTermUrl.searchParams.set('cause', 'CHILDREN_CHANGED');

        this._requestsRemaining = 0;

        console.log(this.id + ': Trip initialized');
    }

    /**
     * @param {Cheerio} $
     */
    parse($) {

        this.name = $('#main h1[itemprop="name"]').text().trim();

        let stars = $('#main header.container p > span.stars-icons');
        if (stars) {
            let num = Number.parseInt(stars.attr('stars'));
            if (num && !Number.isNaN(num)) {
                this.accommodation.classification = num;
            }
        }

        let destination = $('#main header.container p > span:last-child').text();
        if (destination) {
            this.destination = destination.trim().split(', ');
        }

        let travelAgency = $('#main #ta-info > p > a').text();
        if (travelAgency) {
            this.travelAgency = travelAgency.trim();
        }

        let hotelName = $('#unofficial-info > p > b:first-child').text();
        if (hotelName) {
            let starMatch = hotelName.match(/(\*{1,5})/);
            if (starMatch && starMatch[1]) {
                hotelName = hotelName.replace(starMatch[1], '');
            }
            this.accommodation.name = hotelName.trim();
        }

        let hotelUrl = $('#unofficial-info > p > small > a').attr('href');
        if (hotelUrl) {
            this.accommodation.url = hotelUrl;
        }

        let coords = $('#unofficial-info div a[href*="maps.google"]').text();
        if (coords) {
            coords = coords.trim().split(',');
            this.accommodation.lat = Number.parseFloat(coords[0].trim());
            this.accommodation.lon = Number.parseFloat(coords[1].trim());
        }

        let rating = $('#recenze div.over-all-rating').text();
        if (rating) {
            rating = Number.parseInt(rating.split('/')[0]);
            if (rating && !Number.isNaN(rating)) {
                this.accommodation.rating = rating;
            }
        }

        console.log(this.id + ': Trip main data scraped');
        console.log(this.id + ':     name: ' + this.name);
        console.log(this.id + ':     url: ' + this.url);
        console.log(this.id + ':     ta: ' + this.travelAgency);
        console.log(this.id + ': Searching possible terms for this trip');

        this._requestsRemaining++;
        this._scraper.crawler.queue({
            uri: API_SEARCH_TERMS_URL + '?allTerms=true&index=0&page=1/2&pageType=TOUR&tourId=' + this.id,
            priority: 3,
            jQuery: false
        });
    }

    parseTermList(res, url) {
        console.log(this.id + ': Found trip terms, queueing for details');

        this._requestsRemaining--;
        const json = JSON.parse(res.body);

        for (let o of json.terms.splice(0, 15)) {
            for (let t of o.terms) {
                this.queueTermDetail(t.id);
            }
        }
    }

    parseTermDetail(res, url) {

        this._requestsRemaining--;

        const json = JSON.parse(res.body);
        const people = PEOPLE_VARIANTS[res.options._peopleVariant];

        if (json.capacityCheckResult !== 'OK') {
            console.log(this.id + ': Term ' + json.term.term_id + ' variant ' + people.adults + '+' + people.children + ' is over capacity');
            return; // capacity is over, do not include this term
        }

        this.terms.push({
            id: [this.id, Number(json.term.term_id), people.adults, people.children].join('+'),
            url: json.displayLinkUrl,
            from: json.term.departure,
            to: json.term.arrival,
            people: people,
            boarding: TripPage.mapBoarding(json.term.boarding),
            price: json.priceTotal.localizedPrice,
            accommodationType: TripPage.mapAccommodation(json.typKapacity)
        });

        console.log(this.id + ': Term added');
        console.log(this.id + ':     id: ' + json.term.term_id);
        console.log(this.id + ':     people: ' + people.adults + '+' + people.children);

        this.queueTermDetail(res.options._termId, res.options._peopleVariant + 1);
    }

    /**
     * @param {number} termId
     * @param {number} [peopleVariant]
     * @return {boolean} success
     */
    queueTermDetail(termId, peopleVariant) {
        if (peopleVariant === undefined) {
            peopleVariant = 0;
        }

        if (!PEOPLE_VARIANTS[peopleVariant]) {
            return false;
        }

        let people = PEOPLE_VARIANTS[peopleVariant];

        console.log(this.id + ': Queueing term ' + termId + ' variant ' + people.adults + '+' + people.children);

        this._loadTermUrl.searchParams.set('termId', termId + '');
        this._scraper.queueNext(this._loadTermUrl, people, {
            priority: 4,
            jQuery: false,
            _termId: termId,
            _peopleVariant: peopleVariant
        });

        this._requestsRemaining++;
        return true;
    }

    json() {
        let obj = {};
        for (let key of Object.keys(this)) {
            if (!key.startsWith('_')) {
                obj[key] = this[key];
            }
        }
        return JSON.stringify(obj, null, 2);
    }

    isComplete() {
        return this._requestsRemaining <= 0;
    }

    static mapBoarding(boarding) {
        let b = BOARDING_MAP[boarding];
        if (b) {
            return b;
        }
        return Boarding.NONE;
    }

    static mapAccommodation(type) {
        let a = ACCOMMODATION_MAP[type];
        if (a) {
            return a;
        }
        return AccommodationType.APARTMENT;
    }
}