"use strict";

const PageScraper = require('../PageScraper');
const TripPage = require('./TripPage');

module.exports = class ScraperZajezdy extends PageScraper {

    constructor(crawler) {
        super(crawler);

        this.handlers = [
            {
                fn: (...vars) => this.handleTrip(...vars),
                path: /^\/dovolena-.+-z([0-9]+)\/(?:([0-9]+)\/)?$/ // /dovolena-hotel-king-tut-aqua-park-beach-resort-hurghada-egypt-z1473376/
            },
            {
                fn: (...vars) => this.handleSearchResults(...vars),
                path: /^\/dovolena\/$/ // /dovolena/
            },
            {
                fn: (...vars) => this.handleApiTermDetail(...vars),
                path: /^\/api\/detail\/loadDetail(?:Terms)?$/ // /api/detail/loadDetail or loadDetailTerms
            }
        ];

        this.trips = {};
    }


    /**
     * @param {Cheerio} $
     * @param {URL} url
     */
    handleSearchResults({$}, url) {

        let currentPage = Number.parseInt(url.searchParams.get('page'));
        if (isNaN(currentPage)) {
            currentPage = 0;
        }

        console.log('Parsing page ' + currentPage);
        let incompleteTrips = Object.keys(this.trips);
        if (incompleteTrips && incompleteTrips.length) {
            console.warn('There are ' + incompleteTrips.length + ' incomplete trips from previous page');
        }

        $('#results div.result-item').each((i, block) => {
            block = $(block);
            this.queueNext(block.find('h2 > a').attr('href').trim());
        });

        if (currentPage >= 400) {
            return;
        }

        // queue next page
        url.searchParams.set('page', (currentPage + 1) + '');
        this.queueNext(url);
    }

    /**
     * @param {Cheerio} $
     * @param {URL} url
     * @param {RegExpMatchArray} match
     */
    handleTrip({$}, url, match) {

        const tripId = Number.parseInt(match[1]);

        /**
         * @type {TripPage} trip
         */
        let trip = this.trips[tripId];
        if (!trip) {
            trip = new TripPage(this, tripId);
            trip.url = url.protocol + '//' + url.host + url.pathname;
            this.trips[tripId] = trip;
        }

        trip.parse($);
        this._checkCompleteTrip(trip);
    }

    /**
     * @param res
     * @param {URL} url
     */
    handleApiTermDetail(res, url) {

        const tourId = Number(url.searchParams.get('tourId'));
        const termId = Number(url.searchParams.get('termId'));

        let trip = this.trips[tourId];
        if (!trip) {
            console.error('Cannot handle term api response for trip ' + tourId + ', it does not exists');
            return;
        }

        if (url.pathname.includes('loadDetailTerms')) {
            trip.parseTermList(res, url);
        } else {
            trip.parseTermDetail(res, url);
        }

        this._checkCompleteTrip(trip);
    }

    /**
     * @param {string|URL} url
     * @param {{adults: number, children: number, [childrenAge]: [number]}} people
     * @param {object} [options]
     */
    queueNext(url, people, options) {
        if (typeof url === 'string') {
            url = new URL(url);
        }

        if (!people) {
            people = {
                adults: 2,
                children: 0
            };
        }

        if (!people.childrenAge) {
            people.childrenAge = [16, 14, 10, 5];
        }

        url.searchParams.set('dospelych', people.adults + '');
        url.searchParams.set('deti', people.children + '');

        if (people.children) {
            url.searchParams.delete('vekDeti');
            for (let age of people.childrenAge) {
                url.searchParams.append('vekDeti', age + '');
            }
        }

        if (!options) {
            options = {};
        }

        url = url.toString();

        options.uri = url;

        //console.log('Adding url to queue: ' + url);
        this.crawler.queue(options);
    }

    /**
     * @param {TripPage} trip
     * @private
     */
    _checkCompleteTrip(trip) {
        if (!trip.isComplete()) {
            return;
        }

        console.log(trip.id + ': Trip data are complete');

        delete this.trips[trip.id];
        this.onDataCallback(trip.json());
    }

    static getBaseUrl() {
        return 'https://www.zajezdy.cz/';
    }
}