"use strict";

const Crawler = require('crawler');

module.exports = class PageCrawler {

    constructor() {

        this.crawler = new Crawler({
            rateLimit: 2000,
            callback: (...vars) => {
                this.__crawlCallback(...vars)
            }
        });

        this.pageScrapers = {};
    }

    handleUrl(baseUrl, scraper) {
        this.pageScrapers[baseUrl] = scraper;
    }

    queue(url) {
        this.crawler.queue(url);
    }

    __crawlCallback(err, res, done) {
        /**
         * @type {URL}
         */
        const url = new URL(res.request.uri.href);
        const pageScraper = this.getScraperForUrl(url);
        if (!pageScraper) {
            return;
        }

        let handleResult = pageScraper.handlePage(res, url);
        if (handleResult === true || handleResult === undefined) {
            done();
            return;
        }

        if (!handleResult) {
            console.error('Scraper ' + pageScraper.constructor.name + ' was unable to handle path ' + url.pathname);
            done();
            return;
        }

        if (handleResult.then) {
            // result is promise
            handleResult.then(() => {
                done();
            });
        }
    }

    /**
     * @param {URL} url
     * @return {PageScraper|null}
     */
    getScraperForUrl(url) {
        const baseUrl = url.protocol + '//' + url.host + '/';
        const pageScraper = this.pageScrapers[baseUrl];
        if (!pageScraper) {
            console.error('Page scraper for ' + baseUrl + ' not found');
            return null;
        }
        return pageScraper
    }
}