"use strict";

const HTTP = require('http');

const PageCrawler = require('./PageCrawler');
const ScraperZajezdy = require('./scraper/zajezdy/ScraperZajezdy');

module.exports = class App {

    constructor() {
        this.crawler = new PageCrawler();

        {
            const scraper = new ScraperZajezdy(this.crawler);
            scraper.onDataCallback = (...vars) => {this.__onZajezdyData(...vars)};
            this.crawler.handleUrl(ScraperZajezdy.getBaseUrl(), scraper);
        }
    }

    start() {
        this.crawler.queue('https://www.zajezdy.cz/dovolena');
    }

    __onZajezdyData(json) {

        console.log('Sending data to the backend');

        const options = {
            hostname: 'localhost',
            port: 8081,
            path: '/crawler/trip',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': json.length
            }
        };

        const req = HTTP.request(options, res => {
            console.log(`response status code: ${res.statusCode}`)
        });

        req.on('error', error => {
            console.error(error)
        });

        req.write(json);
        req.end();
    }
}